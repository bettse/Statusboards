/* globals d3, $, console, moment, Skycons*/

window.onload = init;

var IsiPhone = navigator.userAgent.indexOf("iPhone") !== -1,
  IsiPod = navigator.userAgent.indexOf("iPod") !== -1,
  IsiPad = navigator.userAgent.indexOf("iPad") !== -1,
  Safari = navigator.userAgent.indexOf("Safari") !== -1,
  IsiPhoneOS = IsiPhone || IsiPad || IsiPod,
  deploy_times = [
      {hours: 10, minutes: 30},
      {hours: 13},
      {hours: 15, minutes: 30}
  ];//Times in military format

function init() {
    if(IsiPhoneOS && Safari) {
        $('#laterdeploy').html('<a href=' + makePanicUrl('NRNextDeploy') + '>Add to Statusboard</a>');
    } else {
        if (!IsiPhoneOS) {
            $(document.body).addClass('contained');
        }
        main();
        setInterval(main, 30000);
    }
    var rpm_status = "http://newrelic.com/status/info.json";

    $.ajax(rpm_status, {dataType: 'jsonp'}).done(statusCallback);
}

function statusCallback(data) {
    //Guards
    if (typeof data === "string") data = $.parseJSON(data);
    if (data.length < 1) return;

    //vars
    var commit = data[0],
    now = moment(),
    last_commit = moment(commit.last_commit);

    //work
    $('#lastdeploy').html(last_commit.from(now));
}

function main() {
    //Show minutes until next deploy large; red if < 30, minutes to next deploy below and smaller
    var now = moment(),
    until = deploy_times.map(function(v, i, a) {
        return moment(v).add('days', (moment(v).isAfter() ? 0 : 1)); //If we've passed a deploy time, calculate it for tomorrow
    }, this).sort();
    //console.log(until.map(function(m) { return m.from(now); }));

    $('#nextdeploy').html(until[0].from(now));
    $('#laterdeploy').html(until[1].from(now));

    //console.log(until[0].diff(now, 'minutes'));
    if (until[0].diff(now, 'minutes') < 30) {
        $('#nextdeploy').addClass('soon');
    }
}

function getParams() {
    var params = {};
    var query = window.location.search.replace("?", "").replace("/", "");
    var vars = query.split('&');
    for (var i=0; i<vars.length; i++) {
        var pair = vars[i].split('=');
        if (pair.length == 2) {
            var key = decodeURIComponent(pair[0]);
            var value = decodeURIComponent(pair[1]);
            if (key && value) {
                params[key] = value;
            }
        }
    }
    return params;
}

function makePanicUrl(source) {
    return 'panicboard://?url=' + encodeURIComponent(document.URL) + '&panel=diy&sourceDisplayName=' + encodeURIComponent(source);
}

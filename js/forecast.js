/* globals d3, $, console, moment, Skycons*/

window.onload = init;

var IsiPhone = navigator.userAgent.indexOf("iPhone") !== -1,
  IsiPod = navigator.userAgent.indexOf("iPod") !== -1,
  IsiPad = navigator.userAgent.indexOf("iPad") !== -1,
  Safari = navigator.userAgent.indexOf("Safari") !== -1,
  IsiPhoneOS = IsiPhone || IsiPad || IsiPod,
  time_format = d3.time.format('%I:%M %p'),
  skycons = new Skycons({"color": "white"});

function init() {

    //desktop: contained + weather
    //iphone browser: link
    //statusboard: weather

    if(IsiPhoneOS && Safari) {
        $('#currentsummary').html('<a href=' + makePanicUrl('Forecast') + '>Add to Statusboard</a>');
    } else {
        if (!IsiPhoneOS) {
            $(document.body).addClass('contained');
        }
        if(navigator && navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(handleGetCurrentPosition, onError);
            setInterval(init, 60000);
        }
    }
}

function handleGetCurrentPosition(location){
    var lat = location.coords.latitude, lon = location.coords.longitude,
    apikey = 'ec3dca9f9390c21b168dd5ff08a8b294',
    url = 'https://api.forecast.io/forecast/' + apikey  + '/' + lat + ',' + lon + '?callback=?',
    query = {exclude: 'hourly,daily,alerts,flags'};
    $.getJSON(url, query, forecastCallback);
}

function onError(){}


function forecastCallback(data) {
    $('#currentsummary').html(data.currently.summary);
    $('#hoursummary').html(data.minutely.summary);
    $('#temperature').html(Math.round(data.currently.temperature, 0) + '&deg;F');
    $('#time').html(time_format(new Date(data.currently.time * 1000)));
    var skycon_method = data.currently.icon.replace(/-/g, '_').toUpperCase();
    skycons.set("currenticon", Skycons[skycon_method]);
    skycons.play();
}


function getParams() {
    var params = {};
    var query = window.location.search.replace("?", "").replace("/", "");
    var vars = query.split('&');
    for (var i=0; i<vars.length; i++) {
        var pair = vars[i].split('=');
        if (pair.length == 2) {
            var key = decodeURIComponent(pair[0]);
            var value = decodeURIComponent(pair[1]);
            if (key && value) {
                params[key] = value;
            }
        }
    }
    return params;
}

function makePanicUrl(source) {
    return 'panicboard://?url=' + encodeURIComponent(document.URL) + '&panel=diy&sourceDisplayName=' + encodeURIComponent(source);
}

/* globals d3, $, console, moment, Skycons*/

window.onload = init;

var IsiPhone = navigator.userAgent.indexOf("iPhone") !== -1,
  IsiPod = navigator.userAgent.indexOf("iPod") !== -1,
  IsiPad = navigator.userAgent.indexOf("iPad") !== -1,
  Safari = navigator.userAgent.indexOf("Safari") !== -1,
  IsiPhoneOS = IsiPhone || IsiPad || IsiPod,
  api_key = "cp8zs3wa6nhf7ewfhehkrfh9",
  todayFormat = d3.time.format.utc('%Y-%m-%d'),
  showtimeFormat = d3.time.format.utc('%I:%M %p');

function init() {
    if(IsiPhoneOS && Safari) {
        d3.select('.contents').append('div').html('<a href=' + makePanicUrl('Movies') + '>Add to Statusboard</a>');
    } else {
        if (!IsiPhoneOS) {
            d3.select('body').classed('contained', true);
        }
        main();
        setInterval(main, 15 * 60000);
    }
}

function main() {
    if(navigator && navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(handleGetCurrentPosition, onError);
    }
}

function onError(){}

function handleGetCurrentPosition(location) {
    var lat = location.coords.latitude, lon = location.coords.longitude,
    url = "http://data.tmsapi.com/v1/movies/showings?startDate=" + todayFormat(new Date()) + "&lat=" + lat + "&lng=" + lon + "&api_key=" + api_key + "";
    d3.json(url, apiCallback);
}

function apiCallback(error, data) {
    if (error) { return console.warn(error); }
    var maxMovies = 5, maxShowtimes = 2;
    data = data.sort(showtimes).slice(0,maxMovies);

    // Update
    var d = d3.select(".contents").selectAll("div").data(data, function(d) { return d.title;});

    // Enter
    var m = d.enter().append("div").attr('class', 'movie');
    m.append('p').text(function(d) { return d.title; });

    m.selectAll('.showtime')
        .data(function(d) {
            return d.showtimes.slice(0, maxShowtimes);
        })
        .enter()
        .append('span')
        .attr('class', 'showtime')
        .text(function(s, i) {
            var output = (i === 0) ? s.theatre.name + ' - ' : '';
            return output += showtimeFormat(new Date(s.dateTime)) + ' ';
        });

    // Exit
    d.exit().remove();
}

function makePanicUrl(source) {
    return 'panicboard://?url=' + encodeURIComponent(document.URL) + '&panel=diy&sourceDisplayName=' + encodeURIComponent(source);
}

function showtimes(a,b) {
    return (b.showtimes.length - a.showtimes.length);
}

